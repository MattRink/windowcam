#!/usr/bin/env python3

import logging
import sys
from argparse import ArgumentParser
from datetime import datetime, timedelta
# from gcloud import storage
from os import path
from pathlib import Path
from time import sleep

has_camera = True
try:
    from picamera2 import Picamera2
except ImportError:
    has_camera = False
    logging.error('Not loaded the picamera2 module, unable to find it.')

def wait(interval: int, on_the_minute: bool = False):
    # Calculate the delay to the start of the next minute
    next_tick = (datetime.now() + timedelta(seconds=interval))
    if on_the_minute:
        # if the interval is less than a minute and you want this tick to run on the minute
        # then you will potentially end up with a time in the past that you can never get to.
        # to get around this we always push the tick into the next minute.
        if interval < 60:
            next_tick = next_tick+ timedelta(minutes=1)
        next_tick = next_tick.replace(second=0)
    delay = (next_tick.replace(microsecond=0) - datetime.now()).seconds
    logging.info(f'Waiting {delay} seconds until {next_tick}')
    sleep(delay)

# def push_to_storage(bucket:str, filepath: str):
#     logging.info(f'Attempting to push {filepath} to storage.')
#     filename = path.basename(filepath)
#     client = storage.Client()
#     bucket = client.get_bucket(bucket)
#     blob = bucket.blob(filepath)
#     blob.upload_from_filename(filename)

def main(image_directory: str, log_directory: str, interval: int, brightness: float):
    logging.info(f'Starting windowcam. Capturing images every {interval} seconds and storing images in {image_directory}')
    logging.info(f'Logging to {log_directory}')
    logging.info(f'Camera settings, brightness: {brightness}')
    wait(interval, on_the_minute=True)

    while True:
        now = datetime.now()
        date_directory = now.strftime('%Y-%m-%d')
        filename = f'{image_directory}/{date_directory}/image-' + now.strftime('%Y-%m-%d-%H-%M-%S') + '.jpg'
        logging.info(f'Capturing {filename}')

        Path(f'{image_directory}/{date_directory}').mkdir(parents=True, exist_ok=True)
        
        if has_camera:
            try:
                with Picamera2() as camera:    
                    logging.debug('Setting up camera')
                    camera_config = camera.create_still_configuration(main={"size": (1280, 720)}, lores={"size": (640, 480)}, display="lores")
                    logging.debug('Configuring camera')
                    camera.configure(camera_config)
                    logging.debug('Starting camera')
                    camera.start()
                    logging.debug('Sleeping')
                    sleep(2) # allow time for the camera to warm up
                    logging.debug('Capturing')
                    camera.capture_file(filename)
                    logging.debug('Captured')
            except RuntimeError as e:
                logging.error(str(e))

        logging.info('Repeating')
        # we wait for 1 second in case the picture was taken within 1 second
        # otherwise we'll immediately take another with the same timestamp
        sleep(1)
        wait(interval)

parser = ArgumentParser(description='Continuously take pictures out of the window.')
parser.add_argument('-d', '--directory', help='the directory to store the images in', default='.')
parser.add_argument('-i', '--interval', help='the interval between taking pictures', default=60, type=int)
parser.add_argument('-l', '--logs', help='the directory to store logs in', default='var/log')
parser.add_argument('-b', '--brightness', help='the brightness level to apply to the camera', default=50, type=int)
args = parser.parse_args()

directory = args.directory.rstrip('/')
log_directory = args.logs.rstrip('/')
interval = args.interval
brightness = args.brightness

Path(directory).mkdir(parents=True, exist_ok=True)
Path(log_directory).mkdir(parents=True, exist_ok=True)

logging.basicConfig(
    filename=log_directory + '/windowcam.log', 
    format="%(asctime)s [%(levelname)s] %(message)s",
    encoding='utf-8', 
    level=logging.INFO
)
logging.getLogger().addHandler(logging.StreamHandler()) # ensure that we log to stderr too

if __name__ == '__main__':
    try:
        main(directory, log_directory, interval, brightness)
    except KeyboardInterrupt:
        logging.info('Exiting by user request.')
        sys.exit(0)
